package com.tmobile.poc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmobile.poc.repository.TransactionStatementDAORepository;
import com.tmobile.poc.vo.StatementVO;

@Service
public class StatmentService {
	@Autowired
	private TransactionStatementDAORepository repository;
	
	
	public StatementVO getStatementsById(Integer statementId)
	{
		 return repository.findStatementsById(statementId);
		
	}

	
	public StatementVO getStatementsByaccountId(String accountId)
	{
		  return repository.findStatementsByaccountId(accountId);
	}
	
	public StatementVO saveStatement(StatementVO statement) {


		
		return repository.insert(statement);
	}

}
