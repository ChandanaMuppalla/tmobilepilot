package com.tmobile.poc.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.tmobile.poc.vo.StatementVO;
@Repository
public interface TransactionStatementDAORepository  extends MongoRepository<StatementVO,String>{
	
	 @Query("{'statementId':?0}")

     StatementVO findStatementsById(@Param("statementId") Integer statementId);
	
	 @Query("{'accountId':?0}")

    StatementVO findStatementsByaccountId(@Param("accountId") String accountId);
	
	
	
	
}
