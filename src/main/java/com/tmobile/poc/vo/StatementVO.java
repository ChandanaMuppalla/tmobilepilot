package com.tmobile.poc.vo;

import org.springframework.data.annotation.Id;
/*import org.springframework.data.annotation.Id;*/
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
/*import org.springframework.data.mongodb.core.mapping.Field;*/
import org.springframework.stereotype.Component;

@Component
@Document(collection = "Statement")
public class StatementVO {
	private static final long serialVersionUID = 1L;

	@Field("statementId")
	private Integer statementId;
	@Field("accountId")
	private String accountId;

	private double BalanceFwd;

	private double TotalStatementDue;

	public Integer getStatementId() {
		return statementId;
	}

	public void setStatementId(Integer statementId) {
		this.statementId = statementId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		accountId = accountId;
	}

	public double getBalanceFwd() {
		return BalanceFwd;
	}

	public void setBalanceFwd(double balanceFwd) {
		BalanceFwd = balanceFwd;
	}

	public double getTotalStatementDue() {
		return TotalStatementDue;
	}

	public void setTotalStatementDue(double totalStatementDue) {
		TotalStatementDue = totalStatementDue;
	}

	@Override
	public String toString() {
		return String.format("Statement[statementId=%d, AccountId=%s, BalanceFwd='%f', TotalStatementDue='%f']",
				statementId, accountId, BalanceFwd, TotalStatementDue);

	}
}
