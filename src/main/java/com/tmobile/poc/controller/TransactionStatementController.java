package com.tmobile.poc.controller;

import java.net.HttpURLConnection;
//comment

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tmobile.poc.service.StatmentService;
import com.tmobile.poc.vo.StatementVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class TransactionStatementController {

	@Autowired
	private StatmentService service;
	@Autowired
	private StatementVO statement;
	
	@ApiOperation(value = "This method is used for creating the Statement Information.")
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Statement Information is created!"),
			@ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Statement Information is not Stored Properly ") })
	@PostMapping(value = "/api-service/v1/statements/save", produces = "application/json")
	public ResponseEntity<?> createStatement(@RequestBody(required=true) StatementVO statement) {

		statement =service.saveStatement(statement) ;
		return new ResponseEntity(statement, HttpStatus.OK);

	}

	@ApiOperation(value = "This method is used for getting the Statement Information by StatementId!.")
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Statement Information is fetched as Sucess!"),
			@ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Statement Information ") })
	@GetMapping(value = "/api-service/v1/statements/Statement/{statementId}", produces = "application/json")
	public ResponseEntity<?> getStatementById(
			@PathVariable(required = true, value = "statementId") Integer statementId) {
	try{
			statement = service.getStatementsById(statementId);
		}
		catch(Exception e)
		{
		e.printStackTrace();
		}
		return  new ResponseEntity(statement, HttpStatus.OK);
	}



	@ApiOperation(value = "This method is used for getting the Statement Information by AccountId!.")
	@ApiResponses(value = {
			@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Statement Information is fetched as Sucess!"),
			@ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Statement Information ") })
	@GetMapping(value = "/api-service/v1/statements/Account/{accountId}", produces = "application/json")
	public ResponseEntity<?> getStatementByaccountId(
			@PathVariable(required = true, value = "accountId") String accountId) {
		try{
			statement = service.getStatementsByaccountId(accountId);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity(statement, HttpStatus.OK);

	}




}
